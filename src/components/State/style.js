export default [
  {
    selector: "node[id]",
    style: {
      label: "data(id)",
      shape: "data(type)"
    }
  },

  {
    selector: "edge[label]",
    style: {
      label: "data(label)",
      width: 3,
      "z-index": 1,
      "curve-style": "bezier",
      "control-point-step-size": 40,
      "target-arrow-shape": "triangle"
    }
  },

  {
    selector: ".background",
    style: {
      "text-background-opacity": 1,
      color: "#fff",
      "text-background-color": "#888",
      "text-background-shape": "roundrectangle",
      "text-border-color": "#000",
      "text-border-width": 1,
      "text-border-opacity": 1
    }
  },

  {
    selector: ".outline",
    style: {
      color: "#fff",
      "text-outline-color": "#888",
      "text-outline-width": 3
    }
  }
];
