import { Lexer } from "chevrotain";
import Tokens from "./tokens";

const lexer: Lexer = new Lexer(Tokens);

export function getTokensList(input: string): string[] {
  return lexer.tokenize(input).tokens.map((currTok: any) => currTok.image);
}

export default lexer;
