import Lexer from "../lexer";
import Parser from "../parser";
import Visitor from "../visitor";

function check(input) {
  const lexed = Lexer.tokenize(input);
  Parser.input = lexed.tokens;
  const cst = Parser.regex();
  const ast = Visitor.visit(cst);
  return ast;
}

test("Builds sequence ast", () => {
  const input = ".(a,b)";
  expect(check(input)).toStrictEqual({
    type: "operator",
    value: "sequence",
    children: [
      { type: "letter", value: "a" },
      { type: "letter", value: "b" }
    ]
  });
});
test("Builds loop ast", () => {
  const input = "*(a)";
  expect(check(input)).toStrictEqual({
    type: "operator",
    value: "loop",
    children: [{ type: "letter", value: "a" }]
  });
});
test("Builds or ast", () => {
  const input = "|(a,b)";
  expect(check(input)).toStrictEqual({
    type: "operator",
    value: "opt",
    children: [
      { type: "letter", value: "a" },
      { type: "letter", value: "b" }
    ]
  });
});
test("Accepts single letter", () => {
  const input = "a";
  expect(check(input)).toStrictEqual({ type: "letter", value: "a" });
});
test("Accepts epsilon", () => {
  const input = "_";
  expect(check(input)).toStrictEqual({ type: "letter", value: "_" });
});

test("Builds sequence ast and with children", () => {
  const input = ".(_,|(b,c))";
  expect(check(input)).toStrictEqual({
    type: "operator",
    value: "sequence",
    children: [
      { type: "letter", value: "_" },
      {
        type: "operator",
        value: "opt",
        children: [
          { type: "letter", value: "b" },
          { type: "letter", value: "c" }
        ]
      }
    ]
  });
});
