import ndfsm from "../ndfsm";

test("Builds letter fsm", () => {
  const input = "a";
  expect(ndfsm(input)).toBe(`
  alphabet: a
  states: q0,q1
  final: q1
  transitions:
  q0,a --> q1
  end.
  `);
});

test("Builds epsilon fsm", () => {
  const input = "_";
  expect(ndfsm(input)).toBe(`
  alphabet: 
  states: q0,q1
  final: q1
  transitions:
  q0,_ --> q1
  end.
  `);
});

test("Builds sequence fsm", () => {
  const input = ".(a,b)";
  expect(ndfsm(input)).toBe(`
  alphabet: ab
  states: q0,q1,q2
  final: q1
  transitions:
  q0,a --> q2
  q2,b --> q1
  end.
  `);
});

test("Builds or fsm", () => {
  const input = "|(a,b)";
  expect(ndfsm(input)).toBe(`
  alphabet: ab
  states: q0,q1,q2,q3,q4,q5
  final: q1
  transitions:
  q0,_ --> q2
  q2,a --> q3
  q3,_ --> q1
  q0,_ --> q4
  q4,b --> q5
  q5,_ --> q1
  end.
  `);
});

test("Builds loop fsm", () => {
  const input = "*(a)";
  expect(ndfsm(input)).toBe(`
  alphabet: a
  states: q0,q1,q2,q3
  final: q1
  transitions:
  q0,_ --> q1
  q0,_ --> q2
  q2,a --> q3
  q3,_ --> q2
  q3,_ --> q1
  end.
  `);
});
