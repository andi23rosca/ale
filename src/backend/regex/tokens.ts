import { createToken, Lexer, TokenType } from "chevrotain";
export const Whitespace: TokenType = createToken({
  name: "Whitespace",
  pattern: /(\t| )/,
  group: Lexer.SKIPPED
});
export const Comma: TokenType = createToken({ name: "Comma", pattern: /,/ });
export const Letter: TokenType = createToken({
  name: "Letter",
  pattern: /[a-z_]/
});
export const LeftParen: TokenType = createToken({
  name: "LeftParen",
  pattern: /\(/
});
export const RightParen: TokenType = createToken({
  name: "RightParen",
  pattern: /\)/
});
export const Or: TokenType = createToken({
  name: "Or",
  pattern: /\|/
});
export const Dot: TokenType = createToken({
  name: "Dot",
  pattern: /\./
});
export const Star: TokenType = createToken({
  name: "Star",
  pattern: /\*/
});

export default [
  Whitespace,
  Comma,
  Letter,
  LeftParen,
  RightParen,
  Or,
  Dot,
  Star
];
