import { CstParser } from "chevrotain";
import Tokens, {
  Whitespace,
  Comma,
  Letter,
  LeftParen,
  RightParen,
  Or,
  Dot,
  Star
} from "./tokens";

class Parser extends CstParser {
  constructor() {
    super(Tokens);
    this.performSelfAnalysis();
  }

  public regex = this.RULE("regex", () => {
    this.MANY(() => {
      this.OR([
        { ALT: () => this.SUBRULE(this.letter) },
        { ALT: () => this.SUBRULE(this.sequence) },
        { ALT: () => this.SUBRULE(this.opt) },
        { ALT: () => this.SUBRULE(this.loop) }
      ]);
    });
  });
  public letter = this.RULE("letter", () => {
    this.CONSUME(Letter);
  });
  public binary = this.RULE("binary", () => {
    this.CONSUME(LeftParen);
    this.SUBRULE1(this.regex);
    this.CONSUME(Comma);
    this.SUBRULE2(this.regex);
    this.CONSUME(RightParen);
  });
  public unary = this.RULE("unary", () => {
    this.CONSUME(LeftParen);
    this.SUBRULE(this.regex);
    this.CONSUME(RightParen);
  });
  public sequence = this.RULE("sequence", () => {
    this.CONSUME(Dot);
    this.SUBRULE(this.binary);
  });
  public opt = this.RULE("opt", () => {
    this.CONSUME(Or);
    this.SUBRULE(this.binary);
  });
  public loop = this.RULE("loop", () => {
    this.CONSUME(Star);
    this.SUBRULE(this.unary);
  });
}

export default new Parser();
