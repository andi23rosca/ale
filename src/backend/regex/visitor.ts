import Parser from "./parser";
import { CstNode } from "chevrotain";
import ExtractedToken from "@/types/ExtractedToken";
import RegexAstNode from "@/types/RegexAstNode";

class Visitor extends Parser.getBaseCstVisitorConstructor() {
  constructor() {
    super();
    this.validateVisitor();
  }
  letter(ctx: { Letter: ExtractedToken[] }): RegexAstNode {
    return {
      type: "letter",
      value: ctx.Letter[0].image
    };
  }
  binary(ctx: { regex: CstNode[] }): RegexAstNode[] {
    return ctx.regex.map(r => this.visit(r));
  }
  unary(ctx: { regex: CstNode[] }): RegexAstNode[] {
    return [this.visit(ctx.regex[0])];
  }
  sequence(ctx: { Dot: CstNode[]; binary: CstNode[] }): RegexAstNode {
    return {
      type: "operator",
      value: "sequence",
      children: this.visit(ctx.binary)
    };
  }
  opt(ctx: { Or: CstNode[]; binary: CstNode[] }): RegexAstNode {
    return {
      type: "operator",
      value: "opt",
      children: this.visit(ctx.binary)
    };
  }
  loop(ctx: { Star: CstNode[]; unary: CstNode[] }): RegexAstNode {
    return {
      type: "operator",
      value: "loop",
      children: this.visit(ctx.unary)
    };
  }
  regex(ctx: {
    letter: CstNode;
    sequence: CstNode;
    opt: CstNode;
    loop: CstNode;
  }): any {
    if (ctx.letter) {
      return this.visit(ctx.letter);
    }
    if (ctx.sequence) {
      return this.visit(ctx.sequence);
    }
    if (ctx.opt) {
      return this.visit(ctx.opt);
    }
    if (ctx.loop) {
      return this.visit(ctx.loop);
    }
    return false;
  }
}

export default new Visitor();
