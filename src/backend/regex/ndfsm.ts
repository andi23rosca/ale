import { ILexingResult, CstNode } from "chevrotain";
import RegexAstNode from "@/types/RegexAstNode";
import Lexer from "./lexer";
import Parser from "./parser";
import Visitor from "./visitor";
import Transition from "@/types/Transition";

function pushUnique(arr: string[], value: string): string[] {
  if (arr.includes(value)) {
    return arr;
  } else {
    return [...arr, value];
  }
}

export function buildDnf(ast: RegexAstNode): string {
  let _n: number = 0;
  const nextState: () => string = () => "q" + _n++;
  let alphabet: string[] = [];
  const firstState: string = nextState();
  const lastState: string = nextState();
  let states: string[] = [firstState, lastState];
  let transitions: Transition[] = [];
  const epsilon: RegexAstNode = {
    type: "letter",
    value: "_"
  };

  function generateDnfAst(
    ast: RegexAstNode,
    first: string,
    last: string
  ): void {
    const { type, value, children } = ast;
    if (type === "letter") {
      if (value !== "_") {
        alphabet = pushUnique(alphabet, value);
      }
      transitions.push({
        to: last,
        from: first,
        transition: value
      });
    } else if (type === "operator") {
      switch (value) {
        case "sequence":
          {
            const tr: string = nextState();
            states.push(tr);
            if (children) {
              generateDnfAst(children[0], first, tr);
              generateDnfAst(children[1], tr, last);
            }
          }
          break;
        case "loop":
          {
            const l: string = nextState();
            const r: string = nextState();
            states.push(l, r);
            if (children) {
              const letter: RegexAstNode = children[0];
              generateDnfAst(epsilon, first, last);
              generateDnfAst(epsilon, first, l);
              generateDnfAst(letter, l, r);
              generateDnfAst(epsilon, r, l);
              generateDnfAst(epsilon, r, last);
            }
          }
          break;
        case "opt":
          {
            const tl: string = nextState();
            const tr: string = nextState();
            const bl: string = nextState();
            const br: string = nextState();
            states.push(tl, tr, bl, br);
            if (children) {
              generateDnfAst(epsilon, first, tl);
              generateDnfAst(children[0], tl, tr);
              generateDnfAst(epsilon, tr, last);
              generateDnfAst(epsilon, first, bl);
              generateDnfAst(children[1], bl, br);
              generateDnfAst(epsilon, br, last);
            }
          }
          break;
        default:
          break;
      }
    }
  }

  generateDnfAst(ast, firstState, lastState);

  return `
  alphabet: ${alphabet.join("")}
  states: ${states.join(",")}
  final: ${lastState}
  transitions:
  ${transitions.map(t => `${t.from},${t.transition} --> ${t.to}`).join("\n  ")}
  end.
  `;
}

export default function(input: string): string {
  const lexed: ILexingResult = Lexer.tokenize(input);
  Parser.input = lexed.tokens;
  const cst: CstNode = Parser.regex();
  const ast: RegexAstNode = Visitor.visit(cst);

  return buildDnf(ast);
}
