import Parser from "./parser";
import { CstNode } from "chevrotain";
import ExtractedToken from "@/types/ExtractedToken";
import Transition from "@/types/Transition";
import Ast from "@/types/Ast";

class Visitor extends Parser.getBaseCstVisitorConstructor() {
  constructor() {
    super();
    this.validateVisitor();
  }
  alphabet(ctx: { Letter: ExtractedToken[] }): string[] {
    return ctx.Letter ? ctx.Letter.map((t: ExtractedToken) => t.image) : [];
  }
  states(ctx: { State: ExtractedToken[] }): string[] {
    return ctx.State ? ctx.State.map((t: ExtractedToken) => t.image) : [];
  }
  final(ctx: { State: ExtractedToken[] }): string[] {
    return ctx.State ? ctx.State.map((t: ExtractedToken) => t.image) : [];
  }
  transition(ctx: {
    State: ExtractedToken[];
    Transition: ExtractedToken[];
  }): Transition {
    const states: string[] = ctx.State
      ? ctx.State.map((t: ExtractedToken) => t.image)
      : [];
    return {
      from: states[0],
      to: states[1],
      transition: ctx.Transition[0].image
    };
  }
  transitions(ctx: {
    transition: { State: ExtractedToken[]; Transition: ExtractedToken[] }[];
  }): Transition[] {
    return ctx.transition ? ctx.transition.map((t: any) => this.visit(t)) : [];
  }
  dfa(ctx: { Bool: ExtractedToken[] }): Boolean {
    return this.boolean(ctx.Bool);
  }
  boolean(ctx: ExtractedToken[]): Boolean {
    if (ctx[0].image === "n") {
      return false;
    }
    return true;
  }
  finite(ctx: { Bool: ExtractedToken[] }): Boolean {
    return this.boolean(ctx.Bool);
  }
  words(ctx: { word: ExtractedToken[] }): string[] {
    return ctx.word ? ctx.word.map((t: any) => this.visit(t)) : [];
  }
  word(ctx: {
    Bool: ExtractedToken[];
    Word: ExtractedToken[];
  }): { word: string; accepted: Boolean } {
    const word: string = ctx.Word[0].image;
    const accepted: Boolean = this.boolean(ctx.Bool);

    return {
      word,
      accepted
    };
  }

  fsm(ctx: {
    alphabet: CstNode;
    states: CstNode;
    final: CstNode;
    transitions: CstNode;
    dfa: CstNode;
    finite: CstNode;
    words: CstNode;
  }): Ast {
    const alphabet: string[] = this.visit(ctx.alphabet);
    const states: string[] = this.visit(ctx.states);
    const final: string[] = this.visit(ctx.final);
    const transitions: Transition[] = this.visit(ctx.transitions);
    const dfa: Boolean = this.visit(ctx.dfa);
    const finite: Boolean = this.visit(ctx.finite);
    const words: { word: string; accepted: Boolean }[] = this.visit(ctx.words);
    return {
      alphabet,
      states,
      final,
      transitions,
      dfa,
      finite,
      words
    };
  }
}

export default new Visitor();
