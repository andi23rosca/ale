import { Lexer, IMultiModeLexerDefinition } from "chevrotain";
import {
  Whitespace,
  Comment,
  Comma,
  EnterAlphabet,
  Letter,
  EnterStates,
  State,
  EnterFinal,
  EnterTransitions,
  End,
  Transition,
  To,
  Dfa,
  Finite,
  Word,
  Words,
  Bool
} from "./tokens";

const lexerDefinition: IMultiModeLexerDefinition = {
  modes: {
    default_mode: [
      Whitespace,
      Comment,
      EnterTransitions,
      EnterAlphabet,
      EnterStates,
      EnterFinal,
      Dfa
    ],
    test_mode: [Whitespace, Comment, Bool, Finite, End, Words, Comma, Word],
    alphabet_mode: [
      Whitespace,
      Comment,
      EnterStates,
      EnterFinal,
      EnterTransitions,
      Dfa,
      Letter
    ],
    states_mode: [
      Whitespace,
      Comment,
      Comma,
      EnterAlphabet,
      EnterFinal,
      EnterTransitions,
      Dfa,
      State
    ],
    final_mode: [
      Whitespace,
      Comment,
      Comma,
      EnterAlphabet,
      EnterStates,
      EnterTransitions,
      Dfa,
      State
    ],
    transitions_mode: [
      Whitespace,
      Comment,
      Comma,
      To,
      End,
      EnterAlphabet,
      EnterStates,
      EnterFinal,
      Dfa,
      Transition,
      State
    ]
  },
  defaultMode: "default_mode"
};

const lexer: Lexer = new Lexer(lexerDefinition);

export function getTokensList(input: string): string[] {
  return lexer.tokenize(input).tokens.map((currTok: any) => currTok.image);
}

export default lexer;
