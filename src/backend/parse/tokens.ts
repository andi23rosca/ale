import { createToken, Lexer, TokenType } from "chevrotain";

export const Whitespace: TokenType = createToken({
  name: "Whitespace",
  pattern: /(\t| )/,
  group: Lexer.SKIPPED
});
export const Comment: TokenType = createToken({
  name: "Comment",
  pattern: /#.*/,
  group: Lexer.SKIPPED
});
export const Comma: TokenType = createToken({ name: "Comma", pattern: /,/ });

// alphabet
export const EnterAlphabet: TokenType = createToken({
  name: "EnterAlphabet",
  pattern: /alphabet:/,
  push_mode: "alphabet_mode"
});
export const Letter: TokenType = createToken({
  name: "Letter",
  pattern: /[a-z]/
});

// states
export const EnterStates: TokenType = createToken({
  name: "EnterStates",
  pattern: /states:/,
  push_mode: "states_mode"
});
export const State: TokenType = createToken({
  name: "State",
  pattern: /(\{\})|(\{[a-zA-Z0-9]+(,[a-zA-Z0-9]+)*\})|([a-zA-Z0-9]+)/
});

// states
export const EnterFinal: TokenType = createToken({
  name: "EnterFinal",
  pattern: /final:/,
  push_mode: "final_mode"
});

// transitions
export const EnterTransitions: TokenType = createToken({
  name: "EnterTransitions",
  pattern: /transitions:/,
  push_mode: "transitions_mode"
});
export const End: TokenType = createToken({
  name: "End",
  pattern: /end\./,
  pop_mode: true
});
export const Transition: TokenType = createToken({
  name: "Transition",
  longer_alt: State,
  pattern: /[a-z_]/
});
export const To: TokenType = createToken({
  name: "To",
  pattern: /-->/
});

export const Dfa: TokenType = createToken({
  name: "Dfa",
  pattern: /dfa:/,
  push_mode: "test_mode"
});
export const Finite: TokenType = createToken({
  name: "Finite",
  pattern: /finite:/
});
export const Word: TokenType = createToken({
  name: "Word",
  pattern: /[a-z_]+/
});

export const Words: TokenType = createToken({
  name: "Words",
  pattern: /words:/
});
export const Bool: TokenType = createToken({
  name: "Bool",
  pattern: /[yn]/,
  longer_alt: Word
});

export default [
  Whitespace,
  Comment,
  Comma,
  EnterAlphabet,
  Letter,
  EnterStates,
  State,
  EnterFinal,
  EnterTransitions,
  End,
  Transition,
  To,
  Dfa,
  Finite,
  Word,
  Bool,
  Words
];
