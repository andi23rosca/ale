import Ast from "@/types/Ast";
import Fsm from "@/types/Fsm";
import Transition from "@/types/Transition";

function createFSM(ast: Ast): Fsm {
  const nodes: Fsm["nodes"] = ast.states.reduce(
    (a: Fsm["nodes"], state: string) => {
      a[state] = {
        transitions: [],
        final: ast.final.includes(state)
      };
      return a;
    },
    {}
  );

  ast.transitions.forEach((t: Transition) => {
    nodes[t.from].transitions.push({
      to: t.to,
      transition: t.transition
    });
  });
  return {
    ...ast,
    nodes
  };
}

export default createFSM;
