import stringify from "../stringify";

test("Should output proper fsm string", () => {
  const fsm = {
    alphabet: ["a", "b"],
    states: ["{q0,q1}", "{}", "{q2,q3}", "{q0,q1,q2,q3}"],
    final: ["{q2,q3}", "{q0,q1,q2,q3}"],
    words: [{ word: "aa", accepted: false }],
    finite: true,
    dfa: false,
    nodes: {
      "{q0,q1}": {
        final: false,
        transitions: [
          {
            to: "{q2,q3}",
            transition: "a"
          },
          {
            to: "{}",
            transition: "b"
          }
        ]
      },
      "{}": {
        final: false,
        transitions: [
          {
            to: "{}",
            transition: "a"
          },
          {
            to: "{}",
            transition: "b"
          }
        ]
      },
      "{q2,q3}": {
        final: true,
        transitions: [
          {
            to: "{}",
            transition: "a"
          },
          {
            to: "{q0,q1,q2,q3}",
            transition: "b"
          }
        ]
      },
      "{q0,q1,q2,q3}": {
        final: true,
        transitions: [
          {
            to: "{q2,q3}",
            transition: "a"
          },
          {
            to: "{q0,q1,q2,q3}",
            transition: "b"
          }
        ]
      }
    }
  };

  expect(stringify(fsm)).toBe(
    `
  alphabet: ab
  states: {q0,q1},{},{q2,q3},{q0,q1,q2,q3}
  final: {q2,q3},{q0,q1,q2,q3}
  transitions:
  {q0,q1},a --> {q2,q3}
  {q0,q1},b --> {}
  {},a --> {}
  {},b --> {}
  {q2,q3},a --> {}
  {q2,q3},b --> {q0,q1,q2,q3}
  {q0,q1,q2,q3},a --> {q2,q3}
  {q0,q1,q2,q3},b --> {q0,q1,q2,q3}
  end.

  dfa: n
  finite: y
  
  words:
  aa,n
  end.
  `
  );
});
