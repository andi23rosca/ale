import { CstParser } from "chevrotain";
import Tokens, {
  Comma,
  EnterAlphabet,
  Letter,
  EnterStates,
  State,
  EnterFinal,
  EnterTransitions,
  End,
  Transition,
  To,
  Dfa,
  Finite,
  Word,
  Words,
  Bool,
  EnterStack,
  LeftParen,
  RightParen
} from "./tokens";

class Parser extends CstParser {
  constructor() {
    super(Tokens);
    this.performSelfAnalysis();
  }
  public fsm = this.RULE("fsm", () => {
    this.MANY(() => {
      this.OR([
        { ALT: () => this.SUBRULE(this.alphabet) },
        { ALT: () => this.SUBRULE(this.stack) },
        { ALT: () => this.SUBRULE(this.states) },
        { ALT: () => this.SUBRULE(this.final) },
        { ALT: () => this.SUBRULE(this.transitions) },
        { ALT: () => this.SUBRULE(this.dfa) },
        { ALT: () => this.SUBRULE(this.finite) },
        { ALT: () => this.SUBRULE(this.words) }
      ]);
    });
  });
  public alphabet = this.RULE("alphabet", () => {
    this.CONSUME(EnterAlphabet);
    this.MANY(() => {
      this.CONSUME(Letter);
    });
  });
  public stack = this.RULE("stack", () => {
    this.CONSUME(EnterStack);
    this.MANY(() => {
      this.CONSUME(Letter);
    });
  });
  public states = this.RULE("states", () => {
    this.CONSUME(EnterStates);
    this.MANY_SEP({
      SEP: Comma,
      DEF: () => {
        this.CONSUME(State);
      }
    });
  });
  public final = this.RULE("final", () => {
    this.CONSUME(EnterFinal);
    this.MANY_SEP({
      SEP: Comma,
      DEF: () => {
        this.CONSUME(State);
      }
    });
  });
  public transitions = this.RULE("transitions", () => {
    this.CONSUME(EnterTransitions);
    this.MANY(() => {
      this.SUBRULE(this.transition);
    });
    this.CONSUME(End);
  });
  public transition = this.RULE("transition", () => {
    this.CONSUME1(State);
    this.CONSUME1(Comma);
    this.CONSUME1(Transition);
    this.OPTION(() => {
      this.SUBRULE(this.stackop);
    });
    this.CONSUME(To);
    this.CONSUME2(State);
  });
  public stackop = this.RULE("stackop", () => {
    this.CONSUME(LeftParen);
    this.CONSUME1(Transition);
    this.CONSUME(Comma);
    this.CONSUME2(Transition);
    this.CONSUME(RightParen);
  });
  public dfa = this.RULE("dfa", () => {
    this.CONSUME(Dfa);
    this.CONSUME(Bool);
  });
  public finite = this.RULE("finite", () => {
    this.CONSUME(Finite);
    this.CONSUME(Bool);
  });
  public words = this.RULE("words", () => {
    this.CONSUME(Words);
    this.MANY(() => {
      this.SUBRULE(this.word);
    });
    this.CONSUME(End);
  });
  public word = this.RULE("word", () => {
    this.CONSUME1(Word);
    this.CONSUME(Comma);
    this.CONSUME(Bool);
  });
}

export default new Parser();
