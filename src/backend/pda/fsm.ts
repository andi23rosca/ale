import PdaAst from "@/types/PdaAst";
import PdaFsm from "@/types/PdaFsm";
import PdaTransition from "@/types/PdaTransition";

function createFSM(ast: PdaAst): PdaFsm {
  const nodes: PdaFsm["nodes"] = ast.states.reduce(
    (a: PdaFsm["nodes"], state: string) => {
      a[state] = {
        transitions: [],
        final: ast.final.includes(state)
      };
      return a;
    },
    {}
  );

  ast.transitions.forEach((t: PdaTransition) => {
    nodes[t.from].transitions.push({
      to: t.to,
      transition: t.transition,
      stack: t.stack
    });
  });
  return {
    ...ast,
    nodes
  };
}

export default createFSM;
