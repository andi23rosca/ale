import Parser from "../parser";
import Lexer from "../lexer";

function check(input) {
  Parser.input = Lexer.tokenize(input).tokens;
  Parser.fsm();
  return Parser.errors.length;
}

test("Throws no errors on a valid list of tokens", () => {
  const input = `
    # comments
    alphabet: ab
    stack: xy
    states: A1,A2
    final: A2
    transitions:
    A1,a --> A2
    A1,a [x,_] --> A2
    end.
    `;

  expect(check(input)).toBe(0);
});

test("Throws errors on invalid input", () => {
  const input = `
    # comments
    alphabet: ab
    states: A1,werwer 222 -
    `;

  expect(check(input)).toBe(1);
});

test("Throws no errors on a valid list of tests", () => {
  const input = `
      dfa:n
      finite:y
      words:
      bbbbbbbbbb,y
      _,y
      ab,n
      end.
    `;

  expect(check(input)).toBe(0);
});
