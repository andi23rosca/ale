import Lexer, { getTokensList } from "../lexer";

const check = input => getTokensList(input);

test("Skips comment line", () => {
  const input = "# some comment that should be ignored";
  expect(check(input)).toStrictEqual([]);
});
test("Extracts alphabet letters", () => {
  const input = "alphabet: abc";
  expect(check(input)).toStrictEqual(["alphabet:", "a", "b", "c"]);
});
test("Alphabet no letters doesn't crash", () => {
  const input = "alphabet:";
  expect(check(input)).toStrictEqual(["alphabet:"]);
});
test("Extracts stack letters", () => {
  const input = "stack: xyz";
  expect(check(input)).toStrictEqual(["stack:", "x", "y", "z"]);
});
test("Extracts states", () => {
  const input = "states: A1,q1,{},{q1},{q1,q2}";
  expect(check(input)).toStrictEqual([
    "states:",
    "A1",
    ",",
    "q1",
    ",",
    "{}",
    ",",
    "{q1}",
    ",",
    "{q1,q2}"
  ]);
});
test("Extracts final states", () => {
  const input = "final: A1,q1,c";
  expect(check(input)).toStrictEqual(["final:", "A1", ",", "q1", ",", "c"]);
});
test("Extracts transitions", () => {
  const input = `
  transitions:
  A1,a --> A2
  A1,a [_,x] --> A2
  end.
  `;
  expect(check(input)).toStrictEqual([
    "transitions:",
    "A1",
    ",",
    "a",
    "-->",
    "A2",
    "A1",
    ",",
    "a",
    "[",
    "_",
    ",",
    "x",
    "]",
    "-->",
    "A2",
    "end."
  ]);
});
test("Extracts test vectors", () => {
  const input = `
    dfa:n
    finite:y
    words:
    bbbbbbbbbb,y
    _,y
    ab,n
    end.
  `;
  expect(check(input)).toStrictEqual([
    "dfa:",
    "n",
    "finite:",
    "y",
    "words:",
    "bbbbbbbbbb",
    ",",
    "y",
    "_",
    ",",
    "y",
    "ab",
    ",",
    "n",
    "end."
  ]);
});
test("Checks full file input", () => {
  const input = `
    # comments
    alphabet: ab
    states: A1,A2
    final: A2
    transitions:
    A1,a --> A2
    end.

    
    dfa:n
    finite:y
    words:
    bbbbbbbbbb,y
    _,y
    ab,n
    end.
    `;
  expect(check(input)).toStrictEqual([
    "alphabet:",
    "a",
    "b",
    "states:",
    "A1",
    ",",
    "A2",
    "final:",
    "A2",
    "transitions:",
    "A1",
    ",",
    "a",
    "-->",
    "A2",
    "end.",
    "dfa:",
    "n",
    "finite:",
    "y",
    "words:",
    "bbbbbbbbbb",
    ",",
    "y",
    "_",
    ",",
    "y",
    "ab",
    ",",
    "n",
    "end."
  ]);
});
