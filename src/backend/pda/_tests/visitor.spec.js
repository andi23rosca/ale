import Lexer from "../lexer";
import Parser from "../parser";
import Visitor from "../visitor";

function check(input) {
  const lexed = Lexer.tokenize(input);
  Parser.input = lexed.tokens;
  const cst = Parser.fsm();
  const ast = Visitor.visit(cst);
  return ast;
}

test("Extracts the alphabet letters", () => {
  const input = "alphabet: abc";
  expect(check(input).alphabet).toStrictEqual(["a", "b", "c"]);
});
test("Alphabet no letters doesn't crash", () => {
  const input = "alphabet:";
  expect(check(input).alphabet).toStrictEqual([]);
});
test("Extracts states", () => {
  const input = "states: A1,q1,c";
  expect(check(input).states).toStrictEqual(["A1", "q1", "c"]);
});
test("Extracts final states", () => {
  const input = "final: A1,q1,c";
  expect(check(input).final).toStrictEqual(["A1", "q1", "c"]);
});
test("Extracts transitions", () => {
  const input = `
  transitions:
  A1,a --> A2
  A1,a [x,y] --> A2
  A1,a [_,y] --> A2
  end.
  `;
  expect(check(input).transitions).toStrictEqual([
    { from: "A1", to: "A2", transition: "a", stack: { pop: "_", push: "_" } },
    { from: "A1", to: "A2", transition: "a", stack: { pop: "x", push: "y" } },
    { from: "A1", to: "A2", transition: "a", stack: { pop: "_", push: "y" } }
  ]);
});

test("Extracts dfa test", () => {
  const input = `
    dfa:n
    finite:y
    words:
    bbbbbbbbbb,y
    _,y
    ab,n
    end.
  `;
  expect(check(input).dfa).toBe(false);
  expect(check(input).finite).toBe(true);
  expect(check(input).words).toStrictEqual([
    { word: "bbbbbbbbbb", accepted: true },
    { word: "_", accepted: true },
    { word: "ab", accepted: false }
  ]);
});
