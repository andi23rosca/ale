import { Lexer, IMultiModeLexerDefinition } from "chevrotain";
import {
  Whitespace,
  Comment,
  Comma,
  EnterAlphabet,
  Letter,
  EnterStates,
  State,
  EnterFinal,
  EnterTransitions,
  EnterStack,
  LeftParen,
  RightParen,
  End,
  Transition,
  To,
  Dfa,
  Finite,
  Word,
  Words,
  Bool
} from "./tokens";

const lexerDefinition: IMultiModeLexerDefinition = {
  modes: {
    default_mode: [
      Whitespace,
      Comment,
      EnterTransitions,
      EnterAlphabet,
      EnterStates,
      EnterStack,
      EnterFinal,
      Dfa,
      Words
    ],
    test_mode: [Whitespace, Comment, Bool, Finite, Words, End, Comma, Word],
    alphabet_mode: [
      Whitespace,
      Comment,
      EnterStates,
      EnterFinal,
      EnterTransitions,
      EnterStack,
      Dfa,
      Words,
      Letter
    ],
    stack_mode: [
      Whitespace,
      Comment,
      EnterStates,
      EnterFinal,
      EnterTransitions,
      EnterAlphabet,
      Dfa,
      Words,
      Letter
    ],
    states_mode: [
      Whitespace,
      Comment,
      Comma,
      EnterAlphabet,
      EnterFinal,
      EnterTransitions,
      Dfa,
      Words,
      State
    ],
    final_mode: [
      Whitespace,
      Comment,
      Comma,
      EnterAlphabet,
      EnterStates,
      EnterTransitions,
      EnterStack,
      Dfa,
      Words,
      State
    ],
    transitions_mode: [
      Whitespace,
      Comment,
      Comma,
      To,
      End,
      LeftParen,
      RightParen,
      EnterAlphabet,
      EnterStates,
      EnterStack,
      EnterFinal,
      Dfa,
      Words,
      Transition,
      State
    ]
  },
  defaultMode: "default_mode"
};

const lexer: Lexer = new Lexer(lexerDefinition);

export function getTokensList(input: string): string[] {
  return lexer.tokenize(input).tokens.map((currTok: any) => currTok.image);
}

export default lexer;
