import Fsm from "@/types/Fsm";
import Transition from "@/types/Transition";
import Node from "@/types/Node";

export default function(fsm: Fsm): string {
  const { alphabet, states, final, nodes, dfa, finite, words } = fsm;
  const transitions: Transition[] = [];

  states.forEach(state => {
    nodes[state].transitions.forEach(tr => {
      transitions.push({
        ...tr,
        from: state
      });
    });
  });

  return `
  alphabet: ${alphabet.join("")}
  states: ${states.join(",")}
  final: ${final.join(",")}
  transitions:
  ${transitions.map(t => `${t.from},${t.transition} --> ${t.to}`).join("\n  ")}
  end.

  ${dfa === undefined ? "" : `dfa: ${dfa ? "y" : "n"}`}
  ${finite === undefined ? "" : `finite: ${finite ? "y" : "n"}`}
  ${
    words === undefined
      ? ""
      : `
  words:
  ${words.map(w => `${w.word},${w.accepted ? "y" : "n"}`).join("\n  ")}
  end.`
  }
  `;
}
