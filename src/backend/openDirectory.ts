import * as fs from "fs";
import glob from "glob";
import File from "@/types/File.ts";
export default function(dir: string): Promise<File[]> {
  let contents: File[] = [];
  return new Promise((resolve, reject) => {
    glob(`${dir}/*.txt`, (err, files) => {
      files.forEach(file => {
        fs.readFile(file, "utf8", (err, data) => {
          let split: string[] = file.split("/");
          let name: string = split[split.length - 1].split(".txt")[0];

          contents.push({
            name,
            content: data
          });
          if (contents.length === files.length) {
            resolve(contents);
          }
        });
      });
    });
  });
}
