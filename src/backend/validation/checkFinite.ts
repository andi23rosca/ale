import Fsm from "@/types/Fsm";

export default function(fsm: Fsm): { finite: Boolean; words: string[] } {
  const { nodes, states } = fsm;
  let words: string[] = [];

  let finite: boolean = true;

  function traverse(node: string, backStack: string[], word: string): void {
    if (!finite) {
      return;
    }
    let stack: string[] = [...backStack, node];
    const { transitions, final } = nodes[node];
    if (final) {
      words.push(word);
    }
    transitions.forEach((tr: { to: string; transition: string }) => {
      if (stack.includes(tr.to)) {
        finite = false;
        return;
      }
      traverse(tr.to, stack, word + tr.transition);
    });
  }
  traverse(states[0], [], "");

  return { finite, words: finite ? words : [] };
}
