import checkWord from "../PdaCheckWord";

const fsm = {
  states: ["A"],
  nodes: {
    A: {
      final: false,
      transitions: [
        {
          to: "B",
          transition: "a",
          stack: {
            pop: "_",
            push: "x"
          }
        },
        {
          to: "B",
          transition: "b",
          stack: {
            pop: "_",
            push: "_"
          }
        },
        {
          to: "B",
          transition: "c",
          stack: {
            pop: "_",
            push: "y"
          }
        }
      ]
    },
    B: {
      final: false,
      transitions: [
        {
          to: "C",
          transition: "d",
          stack: {
            pop: "_",
            push: "_"
          }
        },
        {
          to: "D",
          transition: "d",
          stack: {
            pop: "x",
            push: "_"
          }
        },
        {
          to: "E",
          transition: "e",
          stack: {
            pop: "_",
            push: "_"
          }
        },
        {
          to: "F",
          transition: "_",
          stack: {
            pop: "x",
            push: "_"
          }
        },
        {
          to: "G",
          transition: "_",
          stack: {
            pop: "_",
            push: "_"
          }
        }
      ]
    },
    C: {
      final: false,
      transitions: [
        {
          to: "H",
          transition: "_",
          stack: {
            pop: "_",
            push: "x"
          }
        }
      ]
    },
    D: {
      final: true,
      transitions: []
    },
    E: {
      final: true,
      transitions: [
        {
          to: "I",
          transition: "f",
          stack: {
            pop: "y",
            push: "_"
          }
        }
      ]
    },
    F: {
      final: true,
      transitions: [
        {
          to: "I",
          transition: "g",
          stack: {
            pop: "_",
            push: "_"
          }
        }
      ]
    },
    G: {
      final: true,
      transitions: [
        {
          to: "J",
          transition: "g",
          stack: {
            pop: "x",
            push: "_"
          }
        },
        {
          to: "K",
          transition: "e",
          stack: {
            pop: "_",
            push: "_"
          }
        },
        {
          to: "L",
          transition: "h",
          stack: {
            pop: "y",
            push: "_"
          }
        }
      ]
    },
    H: {
      final: false,
      transitions: []
    },
    I: {
      final: true,
      transitions: []
    },
    J: {
      final: false,
      transitions: []
    },
    K: {
      final: true,
      transitions: []
    },
    L: {
      final: true,
      transitions: []
    }
  }
};
test("Should accept word", () => {
  expect(checkWord("ad", fsm)).toBe(true);

  expect(checkWord("be", fsm)).toBe(true);
  expect(checkWord("cef", fsm)).toBe(true);
  expect(checkWord("ag", fsm)).toBe(true);
  expect(checkWord("b", fsm)).toBe(true);
  expect(checkWord("ch", fsm)).toBe(true);
});

test("Should not accept word", () => {
  expect(checkWord("ae", fsm)).toBe(false);
});
