import checkFinite from "../checkFinite";

test("Finite that generates words: a", () => {
  const fsm = {
    nodes: {
      q0: {
        final: false,
        transitions: [{ to: "q1", transition: "a" }]
      },
      q1: {
        final: true,
        transitions: []
      }
    },
    states: ["q0", "q1"]
  };
  expect(checkFinite(fsm)).toStrictEqual({
    finite: true,
    words: ["a"]
  });
});
test("Finite that generates words: a, aa, ab", () => {
  const fsm = {
    nodes: {
      q0: {
        final: false,
        transitions: [{ to: "q1", transition: "a" }]
      },
      q1: {
        final: true,
        transitions: [
          { to: "q2", transition: "a" },
          { to: "q3", transition: "b" }
        ]
      },
      q2: {
        final: true,
        transitions: []
      },
      q3: {
        final: true,
        transitions: []
      }
    },
    states: ["q0", "q1"]
  };
  expect(checkFinite(fsm)).toStrictEqual({
    finite: true,
    words: ["a", "aa", "ab"]
  });
});

test("Non finite", () => {
  const fsm = {
    nodes: {
      q0: {
        final: false,
        transitions: [{ to: "q1", transition: "a" }]
      },
      q1: {
        final: true,
        transitions: [{ to: "q1", transition: "b" }]
      }
    },
    states: ["q0", "q1"]
  };
  expect(checkFinite(fsm)).toStrictEqual({
    finite: false,
    words: []
  });
});
test("Non finite complicated", () => {
  const fsm = {
    nodes: {
      q0: {
        final: false,
        transitions: [{ to: "q1", transition: "a" }]
      },
      q1: {
        final: true,
        transitions: [
          { to: "q2", transition: "a" },
          { to: "q3", transition: "b" }
        ]
      },
      q2: {
        final: true,
        transitions: []
      },
      q3: {
        final: true,
        transitions: [{ to: "q0", transition: "a" }]
      }
    },
    states: ["q0", "q1"]
  };
  expect(checkFinite(fsm)).toStrictEqual({
    finite: false,
    words: []
  });
});
