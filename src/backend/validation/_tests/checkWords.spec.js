import checkWords from "../checkWords";

test("Accepts words", () => {
  const fsm = {
    nodes: {
      q0: {
        final: false,
        transitions: [{ to: "q1", transition: "a" }]
      },
      q1: {
        final: true,
        transitions: [
          { to: "q1", transition: "b" },
          { to: "q0", transition: "b" }
        ]
      }
    },
    states: ["q0", "q1"],
    words: [
      { word: "a" },
      { word: "ab" },
      { word: "abbbbb" },
      { word: "b" },
      { word: "c" }
    ]
  };
  expect(checkWords(fsm)).toStrictEqual([
    { word: "a", actual: true },
    { word: "ab", actual: true },
    { word: "abbbbb", actual: true },
    { word: "b", actual: false },
    { word: "c", actual: false }
  ]);
});

test("Epsilon words", () => {
  const fsm = {
    nodes: {
      q0: {
        final: false,
        transitions: [{ to: "q1", transition: "a" }]
      },
      q1: {
        final: true,
        transitions: [{ to: "q0", transition: "_" }]
      }
    },
    states: ["q0", "q1"],
    words: [{ word: "a" }, { word: "ab" }, { word: "aaaa" }]
  };
  expect(checkWords(fsm)).toStrictEqual([
    { word: "a", actual: true },
    { word: "ab", actual: false },
    { word: "aaaa", actual: true }
  ]);
});
