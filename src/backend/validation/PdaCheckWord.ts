import PdaFsm from "@/types/PdaFsm";
import PdaNode from "@/types/PdaNode";

interface ITransition {
  to: string;
  transition: string;
  stack: {
    pop: string;
    push: string;
  };
}

function pushToStack(stack: string[], symbol: string): void {
  if (symbol !== "_") {
    stack.push(symbol);
  }
}
function popFromStack(stack: string[], symbol: string): string | undefined {
  if (symbol === "_") {
    return undefined;
  }
  return stack.pop();
}

export default function(word: string, fsm: PdaFsm): Boolean {
  const { nodes, states } = fsm;
  const stack: string[] = [];
  const letters: string[] = [...word].reverse();
  let accepted: boolean = false;

  function walk(node: PdaNode): void {
    const { transitions, final } = node;
    if (!stack.length && !letters.length && final) {
      accepted = true;
      return;
    }
    if (!letters.length && stack.length) {
      return;
    }

    let precedence: number = 5;
    let last: number = 0;
    let i: number = 0;

    let topStack: string = stack[stack.length - 1];
    let currentLetter: string = letters[letters.length - 1];

    // console.log(currentLetter, topStack);
    while (i !== -1 && i < transitions.length) {
      const tr: ITransition = transitions[i];
      if (tr.transition === currentLetter && topStack === tr.stack.pop) {
        last = i;
        i = -1;
        precedence = 1;
        continue;
      }
      if (tr.transition === currentLetter && tr.stack.pop === "_") {
        if (precedence > 2) {
          precedence = 2;
          last = i;
        }
      }
      if (tr.transition === "_" && tr.stack.pop === topStack) {
        if (precedence > 3) {
          precedence = 3;
          last = i;
        }
      }
      if (tr.transition === "_" && tr.stack.pop === "_") {
        if (precedence > 4) {
          precedence = 4;
          last = i;
        }
      }

      i++;
    }
    const tr: ITransition = transitions[last];
    // console.log(tr);
    if (tr.transition === currentLetter) {
      letters.pop();
    }
    if (tr.stack.pop === topStack) {
      popFromStack(stack, tr.stack.pop);
    }
    pushToStack(stack, tr.stack.push);
    walk(nodes[tr.to]);
  }

  walk(nodes[states[0]]);

  return accepted;
}
