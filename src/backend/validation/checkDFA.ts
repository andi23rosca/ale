import Fsm from "@/types/Fsm";

export default function(fsm: Fsm): Boolean {
  const { nodes, states, alphabet } = fsm;
  let hasEpsilon: Boolean = false;
  return states.every((state: any) => {
    const { transitions } = nodes[state];
    const check: { [key: string]: Boolean } = alphabet.reduce(
      (a: any, c: any) => ({ ...a, [c]: false }),
      {}
    );
    transitions.forEach((tr: { to: string; transition: string }) => {
      if (tr.transition === "_") {
        hasEpsilon = true;
      }
      check[tr.transition] = true;
    });
    return alphabet.every((l: any) => check[l]) && !hasEpsilon;
  });
}
