import Fsm from "@/types/Fsm";
import Node from "@/types/Node";

export default function(
  fsm: Fsm
): { word: string; accepted: Boolean; actual: Boolean }[] {
  const { nodes, states, words } = fsm;

  function checkWord(word: string[], node: Node): Boolean {
    if (word.length === 0) {
      return node.final;
    } else {
      return node.transitions.some(tr => {
        if (tr.transition === word[0]) {
          return checkWord([...word].splice(1, word.length - 1), nodes[tr.to]);
        } else {
          if (tr.transition === "_" && nodes[tr.to] !== node) {
            return checkWord(word, nodes[tr.to]);
          }
          return false;
        }
      });
    }
  }

  return words.map(w => ({
    ...w,
    actual: checkWord([...w.word], nodes[states[0]])
  }));
}
