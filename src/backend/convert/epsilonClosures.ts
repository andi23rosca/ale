import Fsm from "@/types/Fsm";
import Node from "@/types/Node";

interface IStringArrObject {
  [key: string]: string[];
}

export function getEpsilonClosure(
  startState: string,
  nodes: { [key: string]: Node }
): string[] {
  const stack: string[] = [startState];
  function walk(state: string): void {
    nodes[state].transitions
      .filter(t => t.transition === "_" && !stack.includes(t.to))
      .forEach(t => {
        stack.push(t.to);
        walk(t.to);
      });
  }
  walk(startState);
  return stack.sort();
}

export default function(fsm: Fsm): IStringArrObject {
  const { states, nodes } = fsm;
  return states.reduce((a, c) => {
    const closure: string[] = getEpsilonClosure(c, nodes);
    return {
      ...a,
      [c]: closure
    };
  }, {});
}
