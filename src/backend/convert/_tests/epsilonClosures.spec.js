import epsilonClosures from "../epsilonClosures";

test("Return epsilon closures", () => {
  const fsm = {
    states: ["A", "B", "C"],
    nodes: {
      A: {
        final: false,
        transitions: [
          {
            to: "A",
            transition: "1"
          },
          {
            to: "B",
            transition: "_"
          },
          {
            to: "B",
            transition: "0"
          },
          {
            to: "C",
            transition: "0"
          }
        ]
      },
      B: {
        final: false,
        transitions: [
          {
            to: "B",
            transition: "1"
          },
          {
            to: "C",
            transition: "_"
          }
        ]
      },
      C: {
        final: true,
        transitions: [
          {
            to: "C",
            transition: "0"
          },
          {
            to: "C",
            transition: "1"
          }
        ]
      }
    }
  };
  expect(epsilonClosures(fsm)).toStrictEqual({
    A: ["A", "B", "C"],
    B: ["B", "C"],
    C: ["C"]
  });
});
