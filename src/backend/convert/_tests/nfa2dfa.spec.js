import nfa2dfa from "../nfa2dfa";

test("nfa2dfa", () => {
  const fsm = {
    alphabet: ["a", "b"],
    states: ["q0", "q1", "q2", "q3"],
    final: ["q3"],
    nodes: {
      q0: {
        final: false,
        transitions: [
          {
            to: "q1",
            transition: "_"
          }
        ]
      },
      q1: {
        final: false,
        transitions: [
          {
            to: "q2",
            transition: "a"
          },
          {
            to: "q3",
            transition: "a"
          }
        ]
      },
      q2: {
        final: false,
        transitions: [
          {
            to: "q0",
            transition: "b"
          },
          {
            to: "q2",
            transition: "b"
          },
          {
            to: "q3",
            transition: "_"
          }
        ]
      },
      q3: {
        final: true,
        transitions: []
      }
    }
  };
  expect(nfa2dfa(fsm)).toStrictEqual({
    alphabet: ["a", "b"],
    states: ["{q0,q1}", "{}", "{q2,q3}", "{q0,q1,q2,q3}"],
    final: ["{q2,q3}", "{q0,q1,q2,q3}"],
    nodes: {
      "{q0,q1}": {
        final: false,
        transitions: [
          {
            to: "{q2,q3}",
            transition: "a"
          },
          {
            to: "{}",
            transition: "b"
          }
        ]
      },
      "{}": {
        final: false,
        transitions: [
          {
            to: "{}",
            transition: "a"
          },
          {
            to: "{}",
            transition: "b"
          }
        ]
      },
      "{q2,q3}": {
        final: true,
        transitions: [
          {
            to: "{}",
            transition: "a"
          },
          {
            to: "{q0,q1,q2,q3}",
            transition: "b"
          }
        ]
      },
      "{q0,q1,q2,q3}": {
        final: true,
        transitions: [
          {
            to: "{q2,q3}",
            transition: "a"
          },
          {
            to: "{q0,q1,q2,q3}",
            transition: "b"
          }
        ]
      }
    }
  });
});
