export default function powersetString(states: string[]): string {
  return `{${states.join(",")}}`;
}
