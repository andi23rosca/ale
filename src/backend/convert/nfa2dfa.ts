import Fsm from "@/types/Fsm";
import epsilonClosures from "./epsilonClosures";
import Powerset from "./powerset";

export default function(fsm: Fsm): Fsm {
  const { alphabet, states, final, nodes } = fsm;

  const stack: Powerset[] = [];
  let powersetStates: string[] = [];
  const powersetNodes: Fsm["nodes"] = {};
  const visited: {
    [key: string]: boolean;
  } = {};
  const closures: {
    [key: string]: string[];
  } = epsilonClosures(fsm);

  stack.push(new Powerset(closures[states[0]]));

  while (stack.length > 0) {
    const basep: Powerset = stack.pop() || new Powerset([]);
    powersetStates.push(basep.name);
    visited[basep.name] = true;
    powersetNodes[basep.name] = {
      final: basep.states.some(s => final.includes(s)),
      transitions: []
    };

    alphabet.forEach(letter => {
      let arr: string[] = [];
      basep.states.forEach(state => {
        arr.push(
          ...nodes[state].transitions
            .filter(t => t.transition === letter)
            .map(t => t.to)
        );
      });
      const powerset: Powerset = new Powerset(
        arr.reduce((a: string[], c) => [...a, ...closures[c]], [])
      );

      powersetNodes[basep.name].transitions.push({
        to: powerset.name,
        transition: letter
      });
      if (!visited[powerset.name]) {
        stack.push(powerset);
      }
    });
  }

  powersetStates = [...new Set(powersetStates)];

  return {
    ...fsm,
    states: powersetStates,
    final: powersetStates.filter(s => powersetNodes[s].final),
    nodes: powersetNodes
  };
}
