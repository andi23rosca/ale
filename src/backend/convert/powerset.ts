import powersetString from "./powersetString";

export default class Powerset {
  name: string = "";
  states: string[] = [];

  setName(states: string[]): void {
    this.name = powersetString(states);
  }

  constructor(states: string[]) {
    const unique: string[] = [...new Set(states)];
    this.states = unique;
    this.setName(unique);
  }
}
