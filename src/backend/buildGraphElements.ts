import Fsm from "@/types/Fsm";

interface Element {
  data: {
    id?: string;
    type?: string;
    source?: string;
    target?: string;
    label?: string;
  };
}

export default function(fsm: Fsm): Element[] {
  const { nodes, states } = fsm;
  const elements: Element[] = [];
  states.forEach((state: string) => {
    const type: string = nodes[state].final ? "round-rectangle" : "ellipse";
    elements.push({
      data: { id: state, type }
    });
    nodes[state].transitions.forEach((tr: any) => {
      const { transition, to } = tr;
      elements.push({
        data: { source: state, target: to, label: transition }
      });
    });
  });
  return elements;
}
