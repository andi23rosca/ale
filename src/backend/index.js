import Lexer from "./parse/lexer";
import Parser from "./parse/parser";
import Visitor from "./parse/visitor";
import FSM from "./parse/fsm";
import checkDFA from "./validation/checkDFA";

function parseInput(text) {
  const lexed = Lexer.tokenize(text);
  Parser.input = lexed.tokens;

  const cst = Parser.fsm();

  const ast = Visitor.visit(cst);

  const fsm = FSM(ast);

  return fsm;
}

const input = `
  alphabet: ab
  states: q0
  final: q0
  transitions:
  q0,a --> q0
  end.
  dfa: n
  finite: n
  words:
  a,y
  ab,n
  end.
`;
// console.log(checkDFA(parseInput(input)));
