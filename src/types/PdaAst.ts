import PdaTransition from "./PdaTransition";

export default interface PdaAst {
  alphabet: string[];
  stack: string[];
  states: string[];
  final: string[];
  transitions: PdaTransition[];
  dfa: Boolean;
  finite: Boolean;
  words: { word: string; accepted: Boolean }[];
}
