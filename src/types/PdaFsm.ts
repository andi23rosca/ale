import PdaTransition from "./PdaTransition";
import PdaNode from "./PdaNode";

export default interface PdaFsm {
  alphabet: string[];
  states: string[];
  final: string[];
  transitions: PdaTransition[];
  dfa: Boolean;
  finite: Boolean;
  words: { word: string; accepted: Boolean }[];
  nodes: { [key: string]: PdaNode };
}
