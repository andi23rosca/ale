export default interface Transition {
  from: string;
  to: string;
  transition: string;
}
