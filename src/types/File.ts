export default interface File {
  name: string;
  content: string;
}
