import Transition from "./Transition";
import Node from "./Node";

export default interface Fsm {
  alphabet: string[];
  states: string[];
  final: string[];
  transitions: Transition[];
  dfa: Boolean;
  finite: Boolean;
  words: { word: string; accepted: Boolean }[];
  nodes: { [key: string]: Node };
}
