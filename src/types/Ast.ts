import Transition from "./Transition";

export default interface Ast {
  alphabet: string[];
  states: string[];
  final: string[];
  transitions: Transition[];
  dfa: Boolean;
  finite: Boolean;
  words: { word: string; accepted: Boolean }[];
}
