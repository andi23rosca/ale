export default interface PdaNode {
  final: Boolean;
  transitions: {
    to: string;
    transition: string;
    stack: { pop: string; push: string };
  }[];
}
