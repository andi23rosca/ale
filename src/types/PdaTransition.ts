export default interface PdaTransition {
  from: string;
  to: string;
  transition: string;
  stack: {
    push: string;
    pop: string;
  };
}
