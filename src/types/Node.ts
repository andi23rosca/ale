export default interface Node {
  final: Boolean;
  transitions: { to: string; transition: string }[];
}
