export default interface RegexAstNode {
  type: string;
  value: string;
  children?: RegexAstNode[];
}
